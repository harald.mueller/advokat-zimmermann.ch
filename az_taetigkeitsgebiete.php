<?php include_once 'inc.begin.html'; ?>
	        <div class="menubar">
	        	<ul>
	        		<li><a href="./"					><img src="img/menupunkt.png">Home</a></li>
	        		<li><a href="az_zurperson.php"		><img src="img/menupunkt.png">Zur Person</a></li>
	        		<li class="menuPunktAktuell"		><img src="img/menupunkt.png">T&auml;tigkeitsgebiete</li>
	        		<li><a href="az_dokumente.php"	    ><img src="img/menupunkt.png">Dokumente</a></li>
	        		<li><a href="az_kontakt.php"		><img src="img/menupunkt.png">Kontakt</a></li>
	        		<li><a href="az_anfrage.php"		><img src="img/menupunkt.png">Anfrage</a></li>
	        		<li><a href="az_impressum.php"	    ><img src="img/menupunkt.png">Impressum</a></li>
	        	</ul>
	        </div>
	        <div class="textTitel">
		        <h1>T&auml;tigkeitsgebiete</h1>	        
	        </div> 
	        <div class="grau_taetigkeitsgeb"></div>
	        <div class="bildTaetigkeiten">
		        <img alt="A. Zimmermann stehend" src="img/Zimmi_stehend_links.png" width="270">
	        </div>
	        <div class="textTaetigkeiten">
				<h2 style="margin-bottom: 2px;">Beurkundungsrecht</h2>
				<p style="margin-top: 2px;">Notariat. Nach Schweizerischem oder Deutschem Recht</p>
				<h2 style="margin-bottom: 2px;">Gesellschaftsrecht</h2>
				<p style="margin-top: 2px;">Gr&uuml;ndung von Einzelunternehmung, GmbH, Aktiengesellschaft
				<br>bis zur Holding, deren Umwandlungen bis zur Aufl&ouml;sung und Liquidation.
				<h2>Vertragsrecht</h2>
				<h2>Ehe- und Konkubinatsrecht</h2>
				<h2>Erbrecht, Nachlassplanung und -vollzug</h2>
				<h2>Testamentsvollstreckungen</h2>
				<p>Auch forensisch, jedoch vorwiegend beratend t&auml;tig als Notar 
				(auch im Bereich des deutschen Notariats), 
				seien dies Gesellschaftsgr&uuml;ndungen, 
				vom Einzelunternehmen bis zur Holding, 
				Nachlassplanung und -regelung, vom Testament, 
				Ehe- und Erbvertrag bis zur Erbteilung, 
				Patientenverf&uuml;gung, 
				gesellschaftsrechtliche Beurkundungen 
				von der Fusion bis zur Abspaltung. 

	        </div> 

<?php include_once 'inc.ende.html'; ?>