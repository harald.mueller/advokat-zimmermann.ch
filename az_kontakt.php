<?php include_once 'inc.begin.html'; ?>
	        <div class="menubar">
	        	<ul>
	        		<li><a href="./"					    ><img src="img/menupunkt.png">Home</a></li>
	        		<li><a href="az_zurperson.php"		        ><img src="img/menupunkt.png">Zur Person</a></li>
	        		<li><a href="az_taetigkeitsgebiete.php"	><img src="img/menupunkt.png">T&auml;tigkeitsgebiete</a></li>
	        		<li><a href="az_dokumente.php"	        ><img src="img/menupunkt.png">Dokumente</a></li>
	        		<li class="menuPunktAktuell"		    ><img src="img/menupunkt.png">Kontakt</li>
	        		<li><a href="az_anfrage.php"		    ><img src="img/menupunkt.png">Anfrage</a></li>
	        		<li><a href="az_impressum.php"	        ><img src="img/menupunkt.png">Impressum</a></li>
	        	</ul>
	        </div>
	        <div class="textTitel">
		        <h1>Kontakt</h1>
	        </div>
	        <div class="grau_kontakt"></div>
	        <div class="personKontakt">
	        	Kontaktieren Sie uns direkt
	        	<br>oder komfortabel &uuml;ber unser <a href="az_anfrage.php">Anfrageformular</a>
	        	<br>
				<br>Tel. +41 61 263 10 10
				<br><a href="mailto:info&#064;advokat-zimmermann.ch">info&#064;advokat-zimmermann.ch</a>
	        </div> 
     
<?php include_once 'inc.ende.html'; ?>