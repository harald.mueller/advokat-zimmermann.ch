<?php include_once 'inc.begin.html'; ?>
	        <div class="menubar">
	        	<ul>
	        		<li><a href="./"					    ><img src="img/menupunkt.png">Home</a></li>
	        		<li class="menuPunktAktuell"		    ><img src="img/menupunkt.png">Zur Person</li>
	        		<li><a href="az_taetigkeitsgebiete.php"	><img src="img/menupunkt.png">T&auml;tigkeitsgebiete</a></li>
	        		<li><a href="az_dokumente.php"	        ><img src="img/menupunkt.png">Dokumente</a></li>
	        		<li><a href="az_kontakt.php"		    ><img src="img/menupunkt.png">Kontakt</a></li>
	        		<li><a href="az_anfrage.php"		    ><img src="img/menupunkt.png">Anfrage</a></li>
	        		<li><a href="az_impressum.php"	        ><img src="img/menupunkt.png">Impressum</a></li>
	        	</ul>
	        </div>
	        <div class="grau_zurperson"></div>
	        <div class="textTitel">
		        <h1>Zur Person</h1>
	        </div>
	        <div class="textUeber">
	        	<p>Rechtsanwalt seit 1983</p>
	        	<p>Solothurnischer Notar 1985 - 2016</p>
	        	<p>Basel-st&auml;dtischer Notar seit 1987</p>
	        	<p>Kulturinteressiert:
	        	<br>Schauspiel, Oper, Ballett.
	        	<br>Darstellende Kunst, bevorzugt moderne.
	        	<br>Musik, vor allem zeitgen&ouml;ssische.</p>
	        	<p>Mitglied bei den Freunden des Kunstvereins,
	        	<br>der Kulturwerkstatt Kaserne Basel, der Regionalen 
	        	<br>Interessengemeinschaft f�r das Theater, 
	        	<br>des Theatervereins, der alumni und der 
	        	<br>Allgemeinen Lesegesellschaft Basel
	        	</p>
	        </div>
	        <div class="personUeber">
	        	<img src="img/portrait_580px_96ppi.png" width="580">
	        </div> 
      
<?php include_once 'inc.ende.html'; ?>