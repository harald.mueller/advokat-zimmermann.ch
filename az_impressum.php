<?php include_once 'inc.begin.html'; ?>
	        <div class="menubar">
	        	<ul>
	        		<li><a href="./"					    ><img src="img/menupunkt.png">Home</a></li>
	        		<li><a href="az_zurperson.php"		        ><img src="img/menupunkt.png">Zur Person</a></li>
	        		<li><a href="az_taetigkeitsgebiete.php"	><img src="img/menupunkt.png">T&auml;tigkeitsgebiete</a></li>
	        		<li><a href="az_dokumente.php"	        ><img src="img/menupunkt.png">Dokumente</a></li>
	        		<li><a href="az_kontakt.php"		    ><img src="img/menupunkt.png">Kontakt</a></li>
	        		<li><a href="az_anfrage.php"		    ><img src="img/menupunkt.png">Anfrage</a></li>
	        		<li class="menuPunktAktuell"		    ><img src="img/menupunkt.png">Impressum</li>
	        	</ul>
	        </div>
	        <div class="textTitel">
		        <h1>Impressum</h1>
			</div>
			<div class="grau_impressum"></div>
			<div class="bildImpressum">
		        <img alt="A. Zimmermann stehend" src="img/Zimmi_stehend_rechts.png" width="340">
	        </div>
			<div class="textImpressumLinks">
				<p>Alois J. Zimmermann
					<br>lic. iur. Rechtsanwalt und Notar
					<br> 
					<br>Freie Strasse 81 / Münsterberg 1
					<br>Postfach 2262
					<br>CH-4001 Basel 
					<br> 
					<br>Tel. +41 61 263 10 10 
					<br><a href="mailto:info&#064;advokat-zimmermann.ch">info&#064;advokat-zimmermann.ch</a> 
					<br> 
					<br> 
					<br> 
					<br>Sprechzeiten nach Vereinbarung 
				</p>
			</div>  
			<div class="textImpressumRechts">				
			</div>  

<?php include_once 'inc.ende.html'; ?>