<?php include_once 'inc.begin.html'; ?>
	        <div class="menubar">
	        	<ul>
	        		<li><a href="./"					    ><img src="img/menupunkt.png">Home</a></li>
	        		<li><a href="az_zurperson.php"		        ><img src="img/menupunkt.png">Zur Person</a></li>
	        		<li><a href="az_taetigkeitsgebiete.php"	><img src="img/menupunkt.png">T&auml;tigkeitsgebiete</a></li>
	        		<li><a href="az_dokumente.php"	        ><img src="img/menupunkt.png">Dokumente</a></li>
	        		<li><a href="az_kontakt.php"	        ><img src="img/menupunkt.png">Kontakt</a></li>
	        		<li class="menuPunktAktuell"	        ><img src="img/menupunkt.png">Anfrage</li>
	        		<li><a href="az_impressum.php"          ><img src="img/menupunkt.png">Impressum</a></li>
	        	</ul>
	        </div>
	        <div class="textTitel">
		        <h1>Anfrage</h1>
			</div>
			<div class="grau_anfrage"></div>
	        <div class="bildAnfrage">
		        <img alt="A. Zimmermann stehend" src="img/Zimmi_stehend_rechts.png" width="260">
	        </div>			
			<div class="formAnfrage">
				<?php
				if ( isset($_REQUEST["senden"])) {
					$fehlers = "";
					if ($_REQUEST["emailvon"] == null){
						$fehlers = $fehlers."<li class='fehlermeldung'>Ohne E-Mail-Adresse oder Telefonnummer k&ouml;nnen wir Sie nicht so gut kontaktieren.</li>";
					}
				
					if ($fehlers == "") {
						$nachrichtErhalten = "Wir haben Ihre Nachricht erhalten und werden uns bei Ihnen melden.";
						$betreiberSite 	= "www.advokat-zimmermann.ch";
						$empfaenger 	= "info@advokat-zimmermann.ch";
						$sender 		= htmlspecialchars($_REQUEST["emailvon"]);
						$mailtext 		= "<br>Email oder Telefon: ".$_REQUEST["emailvon"]."<br><br>Mitteilung: <b>".$_REQUEST["message"]."</b>";
						$mailtext		.= "<br><br>Unternehmer: <b>".$_REQUEST["Unternehmer"]."</b>";
						$mailtext		.= "<br>Privatperson: <b>".$_REQUEST["Privatperson"]."</b>";
						$mailtext		.= "<br><br>Beurkundungsrecht: <b>".$_REQUEST["Beurkundungsrecht"]."</b>";
						$mailtext		.= "<br>Gesellschaftsrecht: <b>".$_REQUEST["Gesellschaftsrecht"]."</b>";
						$mailtext		.= "<br>Vertragsrecht: <b>".$_REQUEST["Vertragsrecht"]."</b>";
						$mailtext		.= "<br>EheUndKonkubinatsrecht: <b>".$_REQUEST["EheUndKonkubinatsrecht"]."</b>";
						$mailtext		.= "<br><br>Erbrecht: <b>".$_REQUEST["Erbrecht"]."</b>";
						$mailtext		.= "<br>Testament: <b>".$_REQUEST["Testament"]."</b>";

						//Mail an Betreiber der Website						
						mail($empfaenger, "Nachricht von: ".$betreiberSite, "<p>Folgende Anfrage kommt von einem Benutzer der Website ".$betreiberSite.":<br><br>".$mailtext."</p>", "From: website\n" . "Content-Type: text/html; charset=utf-8\n"); 
												
						$_REQUEST["emailvon"] 		= "";
						$_REQUEST["message"]  		= "";
						
						echo "<p style='color: green'><img src='img/check.png'>&nbsp;&nbsp;&nbsp;Vielen Dank! ".$nachrichtErhalten;						
					} else {
						echo "<ul>".$fehlers."</ul>";
					}
				}
				?>
				<form class="form-horizontal" action="#contact-form" method="post">
					<fieldset>
						<p>Ich ben&ouml;tige eine Beratung<br>
						   aus folgenden T&auml;tigkeitsbereichen:</p>
						<div class="control-group">
							<table>
								<tr>
									<td valign="top">
										    <input class="chkBox" type="checkbox" name="Beurkundungsrecht">Beurkundungsrecht 
										<br><input class="chkBox" type="checkbox" name="Gesellschaftsrecht">Gesellschaftsrecht  
										<br><input class="chkBox" type="checkbox" name="Vertragsrecht">Vertragsrecht  
										<br><input class="chkBox" type="checkbox" name="EheUndKonkubinatsrecht">Ehe- und Konkubinatsrecht  
										<br><input class="chkBox" type="checkbox" name="Erbrecht">Erbrecht, Nachlassplanung/-vollzug
										<br><input class="chkBox" type="checkbox" name="Testament">Testamentsvollstreckungen  
									
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>Ich bin <input class="chkBox" type="checkbox" name="Privatperson">Privatperson
									            <input class="chkBox" type="checkbox" name="Unternehmer">Unternehmer
									</td>
								</tr>
							</table>
							<p>Bitte nehmen Sie mit mir Kontakt auf.</p> 
						</div>
						<div class="control-group">
							<label class="control-label" for="email">Absender:</label>
							<div class="controls">
								<input class="input-xlarge" type="text" id="email" name="emailvon" placeholder="E-Mail oder Telefonnummer" value="<?=$_REQUEST["emailvon"]?>"/>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="message">Mitteilung:</label>
							<div class="controls">
								<textarea class="input-xlarge" rows="3" id="message" name="message" placeholder="Ihre Bemerkung..."><?=$_REQUEST["message"]?></textarea>
							</div>
						</div>
						<div class="form-actions">
							<button type="submit" name="senden" id="senden" class="btn">Senden</button>
						</div>
					</fieldset>
				</form>
			</div>
			        
	        <div class="personAnfrage">
	        	
	        </div> 
       
<?php include_once 'inc.ende.html'; ?>