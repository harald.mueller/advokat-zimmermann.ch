<?php 
session_start();
require_once 'functions.php';
function getRelIconDirectory() {	return "icon/";	}
function getRelDataDirectory() {	return "downloads/";			}
function getAbsDataDirectory() {
	return getcwd()."/".getRelDataDirectory();	
}
$passwortOK = false;
if (isset($_REQUEST["btnPasswort"])) {
	if (isset($_REQUEST["inpPasswort"])) {
		$inpPasswort = $_REQUEST["inpPasswort"];
		if ($inpPasswort == "baeumleingasse") {
			$passwortOK = true;
		}
	}
}
if (isset($_REQUEST['btnFileupload'])) {
	$fileName = basename( $_FILES['uploaded']['name']);
	if (strlen($fileName) > 1) {
		uploadFile($fileName);
	}
}
if (isset($_REQUEST['toDelete'])) {
	if ($_REQUEST['toDelete'] != "") {
		deleteFile($_REQUEST['toDelete']);
		$_REQUEST['toDelete'] = "";
	}
}
?> 
<?php include_once 'inc.begin.html'; ?>
	        <div class="menubar">
	        	<ul>
	        		<li><a href="./"					><img src="img/menupunkt.png">Home</a></li>
	        		<li><a href="az_zurperson.php"		><img src="img/menupunkt.png">Zur Person</a></li>
	        		<li><a href="az_taetigkeitsgebiete.php"	><img src="img/menupunkt.png">T&auml;tigkeitsgebiete</a></li>
	        		<li class="menuPunktAktuell"		><img src="img/menupunkt.png">Dokumente</li>
	        		<li><a href="az_kontakt.php"		><img src="img/menupunkt.png">Kontakt</a></li>
	        		<li><a href="az_anfrage.php"		><img src="img/menupunkt.png">Anfrage</a></li>
	        		<li><a href="az_impressum.php"	><img src="img/menupunkt.png">Impressum</a></li>
	        	</ul>
	        </div>
	        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
	        <div class="textTitel">
		        <h1>Dokumente</h1>
	        </div>
	        <div class="grau_dokumente"></div>
	        <div class="textDokumente">
		        <p>Dokumente zum herunterladen:</p>
	        	<form class="listDokumente" method="post">
	        	<?= dokusAnzeigen($passwortOK); ?>
	        	</form>
	    </div>
		
	<div class="clearfix"></div>
	        
<?php include_once 'inc.ende.html'; ?>